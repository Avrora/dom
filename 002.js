/*
  JS DOM Exercise

  1. Mettre le texte de l'élément avec l'id para2 en bleu
  2. Mettre une border en pointillet noire à la section2
  3. Mettre une background color orange à l'élément de la classe colorful de la section2 
  4. Mettre le h2 de la section1 en italique
  5. Cacher l'élément colorful situé dans un paragraphe
  6. Changer le texte de para2 pour "modified by JS"
  7. Changer le href du lien de la section1 pour le faire aller sur simplonlyon.fr
  8. Rajouter la classe bigText sur le h2 de la section2

  Bonus : Faire que tous les paragraphe du document soit en italique

 */

let para2 = document.querySelector("#para2");
para2.style.color = "blue";
para2.innerHTML="Modified by JS"

let section2 = document.querySelector("#section2");
section2.style.border = "dashed black";
section2.style.backgroundColor = "orange";

let h2 = document.querySelector("#section1>h2");
h2.style.fontStyle = "italic";
h2.className = "bigText";

let colorful = document.querySelector(".colorful");
colorful.style.display = 'none';

let section1 = document.querySelector("#section1"); //Ne rabotaet
section1.inheritHREF = "https://www.simplonlyon.fr";

let p = document.querySelector("p");
p.style.fontStyle = "italic";