/*
  JS DOM Exercise

  1. Mettre le texte de l'élément avec l'id para2 en bleu
  2. Mettre une border en pointillet noire à la section2
  3. Mettre une background color orange à l'élément de la classe colorful de la section2 
  4. Mettre le h2 de la section1 en italique
  5. Cacher l'élément colorful situé dans un paragraphe
  6. Changer le texte de para2 pour "modified by JS"
  7. Changer le href du lien de la section1 pour le faire aller sur simplonlyon.fr
  8. Rajouter la classe bigText sur le h2 de la section2

  Bonus : Faire que tous les paragraphe du document soit en italique

 */


let para2 = document.querySelector("#para2");
para2.style.color = "blue";

let section2 = document.querySelector("#section2");
section2.style.border = "dashed";

let colorful = section2.querySelector(".colorful"); // let colorful = document.querySelector("#section2 .colorful");
colorful.style.backgroundColor = "orange";

let italic = document.querySelector("#section1 h2");
italic.style.fontStyle = "italic";

let hide = document.querySelector("p span");
hide.style.display = 'none';

para2.innerHTML="Modified by JS";

let changeHref = document.querySelector("a");
changeHref.href = "https://www.simplonlyon.fr";

colorful.classList.add("bigText"); //colorful.className += "bigTeht"; - plohoi sposob

let paras = document.querySelectorAll("p");
for(let item of paras) {
  item.style.fontStyle = "italic";
}


