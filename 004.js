/*Exercices tableau + boucles
1. Créer un array (tableau) de valeurs textuelles et le mettre dans une variable tab
2. Afficher dans la console la 3ème valeur du tableau
3. Ajouter "modified" à la deuxième valeur du tableau (concaténation)
4. En vous inspirant de la boucle for..of faite dans le fichier exercice-dom.js, parcourir le tableau et afficher en console chacune de ses valeurs
4bis. (optionnel) Faire la même chose avec un for classique (et avec un while ?)
5. Créer une variable dans laquelle vous concaténerez chaque valeur du tableau avec une boucle avant d'afficher la variable
6. Créer une variable count initialisée à 0 puis faire une boucle sur le tableau qui incrémentera cette variable 
SI la valeur actuelle de la boucle fait plus de 3 caractères (le .length permet de compter le nombre de caractères d'une string)*/


let tableau=new Array ("tab", "var1", "var2", "var3"); //Créer un array (tableau) de valeurs textuelles et le mettre dans une variable tab
console.log(tableau[tableau.length-2]); //Afficher dans la console la 3ème valeur du tableau
console.log(tableau[1]+" modified"); //Ajouter "modified" à la deuxième valeur du tableau (concaténation)
for (let value of tableau) { /*En vous inspirant de la boucle for..of faite dans le fichier exercice-dom.js, parcourir le tableau et afficher en console chacune de ses valeurs*/                          
    console.log(value);
  }

let value=0; // Faire la même chose avec un for classique (et avec un while ?)
while (value<tableau.length) {
  console.log(tableau[value]);
  value++;
}



for (let count = 0; tableau[1].length>3; count++;) {
  console.log(count);
}

while(count.length=Math.pow(>3)) {
  count++;
  console.log(count.length);
}