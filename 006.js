/*
Exercice Function + DOM
1. En JS, comme on a déjà fait, changer la couleur du texte du para1 en blue, celle du para2 en red et celle du para3 en orange
2. En regardant le code obtenu, vous devriez remarquer qu'il se répète pas mal, identifiez les morceaux qui change
3. Faire une fonction changeColor qui attendra plusieurs argument (ceux que vous avez identifié) qui permettra de changer 
la couleur de n'importe quel élément en lui donnant n'importe quelle couleur
4. Hors de la fonction, faire une selection et une boucle pour modifier la couleur de tous les élément avec la classe color en vert
5. En utilisant le querySelectorAll et un if, essayer d'ajouter à la fonction changeColor le fait que si plusieurs éléments 
correspondent au sélecteur, ça fera la boucle pour changer la couleur de chacun d'entre eux
*/

//1
let para1 = document.querySelector("#para1");
para1.style.color = "blue";

let para2 = document.querySelector("#para2");
para2.style.color = "red";

let para3 = document.querySelector("#para3");
para3.style.color = "orange";

//3
/*function changeColor(color, cible) {

  if (cible="para1") {
    color="yellow";
  } else if (cible="para2") {
    color="green";
  } else {
    color="gray"
  }
}*/

changeColor("#para1", "blue");
changeColor("#para2", "red");
changeColor("#para3", "orange");

function changeColor(selecteur, color) {
  let element = document.querySelector(selecteur);
  element.style.color = color;
}

//4
let coloredText = document.querySelectorAll(".color");
for(let item of coloredText) {
  item.style.color = "green";
}