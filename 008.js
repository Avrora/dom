/*1. Dans le HTML, créer un button et mettre un event au click dessus qui fera un console.log
  2. Créer ensuite un élément p dans le HTML et faire qu'au click sur le button, le contenu du p change pour devenir "modified by event"
  3. Créer un élément input dans le HTML et faire en sorte de récupérer sa valeur au click sur le bouton 
  et de la console log (il va falloir capturer l'input avec un querySelector et faire un .value pour avoir sa valeur)
  4. Faire en sorte qu'au click sur le bouton, le texte du paragraphe devienne la valeur de l'input
  5. Faire la même chose, mais au lieu que ça se passe au click sur le button, 
  le texte du p change à chaque fois qu'on tape quelque chose dans l'input (event différent, à voir lequel et plus sur le button)*/

let button=document.querySelector("#btn");       //1
let p=document.querySelector("p");               //2
let input = document.querySelector(".form");     //3
btn.addEventListener("click", function() {
  console.log("Bloup");                          //1
  p.textContent = "Modified by event";           //2
  console.log(input.value);                      //3
  p.textContent = input.value;                   //4
});
input.addEventListener("input", function() {     //5
  p.textContent = input.value;
})

