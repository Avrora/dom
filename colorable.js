/*Exercice Event + Function
1. Dans un nouveau fichier exercice-coloring.html / exercice-coloring.js, créer un input type color et un paragraphe avec une class "colorable"(modifié)
2. Dans le javascript, rajouter un event au click sur le paragraphe qui changera sa color en red
3. Dans ce même event, faire en sorte de récupérer la value de l'input et en faire un console.log (comme on a déjà fait avant)
4. Une fois qu'on a récupéré la value de l'input, l'utiliser pour faire que la couleur du paragraphe devienne celle indiquée dans l'input
5. Rajouter 3 autres paragraphes, tous avec la classe "colorable" et faire un querySelectorAll pour ajouter l'eventListener fait précédemment 
sur tous les éléments ayant cette classe (il faudra utiliser une boucle, genre un for...of)
*/


let para = document.querySelector(".colorable");   
let input = document.querySelector("input");             
para.addEventListener("click", function() {
  //para.style.color = 'red';
  console.log(input.value);  
  para.style.color = input.value;
})

