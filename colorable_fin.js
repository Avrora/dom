/*Exercice Event + Function
1. Dans un nouveau fichier exercice-coloring.html / exercice-coloring.js, créer un input type color et un paragraphe avec une class "colorable"(modifié)
2. Dans le javascript, rajouter un event au click sur le paragraphe qui changera sa color en red
3. Dans ce même event, faire en sorte de récupérer la value de l'input et en faire un console.log (comme on a déjà fait avant)
4. Une fois qu'on a récupéré la value de l'input, l'utiliser pour faire que la couleur du paragraphe devienne celle indiquée dans l'input
5. Rajouter 3 autres paragraphes, tous avec la classe "colorable" et faire un querySelectorAll pour ajouter l'eventListener fait précédemment 
sur tous les éléments ayant cette classe (il faudra utiliser une boucle, genre un for...of)
*/


/*let color = document.querySelector(".colorable");   
let inputColor = document.querySelector("#inp");             
color.addEventListener("click", function() {
  //color.style.color = 'red';
  console.log(inputColor.value);  
  color.style.color = inputColor.value;
})*/

let colors = document.querySelectorAll(".colorable"); //Nous inisialise un variable COLORS qui va etre active pour tous elements avec class = colorable
let inputColor = document.querySelector("#inp"); // Nous inisialise un variable INPUTCOLOR qui va etre active pour l'element avec id = inp

for (const item of colors) { //Nachinaem function "for...of". Vvodim peremennuu ITEM qui parcours tout les elements du tableau colors
    item.addEventListener("click", function () { 
      //Mi vvodim obrabotchik sobitii (metod) addEventListener, kotorii srabotaet pri klike. Nachnetsya vipolnenie funkzii
        item.style.color = inputColor.value; //Privyazivaem izmznznie tsveta v ITEM k znacheniu INPUTCOLOR
        console.log(inputColor.value) //Pokazivaet tsvetovoi kod tsveta (INTUTCOLOR) v konsoli
    });
    
}

