/*
Dom create Exercices

1. Créer un élément de type paragraphe et le mettre dans une variable
2. Assigner à cet élément comme textContent "created by js"
3. mettre cet élément dans la section #target qui est dans le HTML
4. Ajouter un event au click sur l'élément paragraphe qui affichera "coucou" en console
5. Rajouter un bouton dans le fichier HTML et lui mettre un event au click qui créera un nouvel élément paragraphe pour le mettre dans la section target
6. Créer une variable count initialisée à 0, et faire qu'à chaque fois qu'on click sur le bouton, elle s'incrémente de 1
7. Assigner au textContent de l'élément créé par le bouton la valeur de la variable count
8. Rajouter un event au double click sur les éléments créés par le bouton qui supprimera 
l'élément en question (.remove()) et qui décrémentera la variable count de 1
*/


let para = document.createElement("p");
let text = document.querySelector("#target");
let button = document.querySelector("button");
let count = 0;
para.textContent = "Created by JS";
text.appendChild(para);

text.addEventListener("click", function() {
    console.log("Coucou!");
});

button.addEventListener("click", function() {
    let para2 = document.createElement("p");
    para2.textContent = "Je counte " + count;
    text.appendChild(para2);
    count++;
    console.log(count);
    para2.addEventListener("dblclick", function() {
        para2.remove(count--);
    });
});


