/*
1. Créer une fonction qui crée un élément paragraphe et l'append dans l'élément avec l'id paragraphe
2. Modifier la fonction pour faire en sorte qu'avec 2 arguments on puisse choisir quel type d'élément créer et où l'ajouter
3. Faire en sorte que la fonction renvoie l'élément créé afin de pouvoir continuer à interagir avec en dehors de celle ci
4. En utilisant cette fonction, créer 3 éléments : 
* une div qui ira dans l'élément #target et qui aura "je suis une div" en textContent
* un p qui ira dans l'élément #target et qui aura "je suis un p" en textContent
* un p qui ira dans l'élément div créé précédemment qui aura "je suis un p dans une div" en textContent

*/

//1, 2
//let ftype = "p";
//let fselector = "#target";

//create(ftype, fselector); //ili mozno bez _let_ vishe: togda - create("p", "#target"); 


let divElement = createWithSelector("div", "#target");
divElement.textContent = "Je suis un div";

let pElement = createWithSelector("p", "#target");
pElement.textContent = "Je suis un p";

let pInDiv = createWithTarget("p", divElement);
pInDiv.textContent = "Je suis un p dans une div";

function createWithSelector(ftype, fselector) { //sami pridymali argumenti
    let element = document.createElement(ftype);
    let target = document.querySelector(fselector);
    target.appendChild(element);
    return element;        //3
}

function createWithTarget(ftype, target) { //sami pridymali argumenti
    let element = document.createElement(ftype);
    target.appendChild(element);
    return element;        //3
}
