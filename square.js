/*Exercice Event
1. Dans un nouveau fichier exercice-square.html / exercice-square.js, une div avec une classe square, 
faire un peu de css faire que la classe square soit un carré rouge (le mettre en position absolute avec top/left 0)(modifié)
2. Dans le javascript, capturer le body de la page et rajouter un eventListener dessus qui se declenchera lorsqu'on appuie sur une touche du clavier
(pour voir si ça marche, faites un ptit console.log de bloup par exemple)
3. Dans la fonction de l'eventListener, rajouter l'argument event et faire un console.log de 
sa propriété code (ça devrait afficher le code de la touche que vous avez pressée)
4. Capturer la div square et dans l'eventListener, faire en sorte que lorsqu'on presse une touche, la propriété left de l'élément soit mise à '30px'
5. Maintenant le carré va spécifiquement à '30px', faisons en sorte qu'il gagne 5px à chaque fois qu'on appuie sur une touche, pour ça, 
on peut récupérer la position actuelle du square avec la propriété .offsetLeft, 
il  "suffit" de lui ajouter 5 puis de concaténer "px" derrière pour faire que le carré se déplace à chaque appuie de touche
6. En utilisant un if, faire que le déplacement ne se fasse que lorsqu'on appuie sur la touche right 
(pour ça, il va falloir vérifier si event.code vaut ArrowRight)
7. Une fois qu'on a réussi à faire ça, faire 3 autres if pour chacune des flêches directionnelles, 
en soustrayant au offsetLeft pour aller vers la gauche, et en additionnant et soustrayant le offsetTop pour aller vers le bas et le haut
*/


/*let square = document.querySelectorAll(".square");
document.querySelector('body').addEventListener('keydown', function(event) {
  //console.log("Bloup");
  console.log(event);
});*/


let square = document.querySelector(".square");
let page = document.querySelector("body");

page.addEventListener('keydown', function(event) {
  console.log(event);
  if (event.code == "ArrowLeft") {
    square.style.left = square.offsetLeft - 30 - 5 + "px";
  };

  if (event.code == "ArrowRight") {
    square.style.left = square.offsetLeft + 30 + 5 + "px";
  };

  if (event.code == "ArrowUp") {
    square.style.top = square.offsetTop - 30 - 5 + "px";
  };

  if (event.code == "ArrowDown") {
    square.style.top = square.offsetTop + 30 + 5 + "px";
  };
});




